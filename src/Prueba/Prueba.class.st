"
Mi clase de prueba.
"
Class {
	#name : #Prueba,
	#superclass : #Object,
	#instVars : [
		'a',
		'b'
	],
	#category : #Prueba
}

{ #category : #'instance creation' }
Prueba class >> newWithColor: aColor [
	^ self new
		a: aColor lighter;
		b: aColor darker;
		yourself
]

{ #category : #accessing }
Prueba >> a [
 ^ a
]

{ #category : #accessing }
Prueba >> a: anObject [
	a := anObject
]

{ #category : #accessing }
Prueba >> b [
	^ b
]

{ #category : #accessing }
Prueba >> b: anObject [
	b := anObject
]
